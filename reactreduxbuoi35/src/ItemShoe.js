import React, { Component } from 'react'
import { connect } from 'react-redux';
import { BUY_SHOE, DETAIL_SHOE } from './redux/constant/shoeConstane';

class ItemShoe extends Component {
  render() {
    let {name,image} = this.props.shoe;
    return (
      <div className='item'>
        <div className="card text-left">
          <img className="card-img-top h-100" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <button className='btn btn-danger' onClick={() => {this.props.handleBuyShoe(this.props.shoe)}}>Buy</button>
            <button className='btn btn-success ml-3' onClick={() => {
              this.props.handleDetail(this.props.shoe)
            }}>Detail</button>
          </div>
        </div>
      </div>
    )
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleBuyShoe: (value) => {
      dispatch({
        type: BUY_SHOE,
        payload:value,
      })
    },
    handleDetail: (value) => {
      dispatch({
        type: DETAIL_SHOE,
        payload:value,
      })
    }
  }
}

export default  connect(null,mapDispatchToProps)(ItemShoe);
