import React, { Component } from 'react'
import { connect } from 'react-redux';
import { ShoeReducer } from './redux/reducer/shoeReducer';

class Detail extends Component {
  render() {
    let {image,name,price,description} = this.props.detail;
    return (
      <div className="container row mx-auto mt-5">
        <div className="col-4">
            <h2>Ảnh sản phẩm</h2>
            <img className='w-100' src={this.props.detail.image} alt="" />
        </div>
        <div className="col-8 text-left">
            <h2 className='mb-5'>Thông tin chi tiết sản phẩm</h2>
            <p className='mt-5'><b>Tên giày: </b>{name}</p>
            <p><b>Giá: </b>{price}</p>
            <p><b>Chi tiết: </b>{description}</p>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return{
        detail: state.ShoeReducer.detail,
    }
}

export default connect(mapStateToProps)(Detail);