import { combineReducers } from "redux";
import { ShoeReducer } from './shoeReducer';

export const rootReducer = combineReducers({
    ShoeReducer,
})