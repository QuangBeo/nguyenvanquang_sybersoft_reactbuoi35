import { dataShoe } from "../../data/data";
import { BUY_SHOE, DETAIL_SHOE, DOWN_NUMBER, UP_NUMBER } from '../constant/shoeConstane';

const initleState = {
    shoeArr: dataShoe,
    cart: [],
    detail: dataShoe[0],
}

export const ShoeReducer = (state = initleState, action) => {
    switch (action.type) {
        case BUY_SHOE: {
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((index) => {
                return index.id == action.payload.id;
            })
            if(index == -1){
                let cartItem = {...action.payload,number:1};
                cloneCart.push(cartItem); 
            }else{
                cloneCart[index].number++;
            }
            return {
                ...state,
                cart: cloneCart,
            }
        }
        case DETAIL_SHOE: {
            state.detail = action.payload;
            return {...state};
        }
        case UP_NUMBER: {
            let cartItemUp = [...state.cart];
            let index = cartItemUp.findIndex((index) => index.id === action.payload.id);
            cartItemUp[index].number++;
            return {...state,cart:cartItemUp};
        }
        case DOWN_NUMBER: {
            const cartItemDown = [...state.cart];
            const index = cartItemDown.findIndex((index) => index.id === action.payload.id);
            if(cartItemDown[index].number >= 1){
                cartItemDown[index].number--;
            }
            if(cartItemDown[index].number == 0){
                cartItemDown.splice(index,1);
            }
            return {...state, cart:cartItemDown};

        }
        default:
            return {...state};
    }
}

