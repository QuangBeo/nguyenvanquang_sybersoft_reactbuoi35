import React, { Component } from 'react'
import Detail from './Detail'
import ListShoe from './ListShoe'
import Cart from './Cart'

export default class ShoeComponent extends Component {
  render() {
    return (
      <div>
        <Cart/>
        <ListShoe/>
        <Detail/>
      </div>
    )
  }
}
