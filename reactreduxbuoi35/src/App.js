import logo from './logo.svg';
import './App.css';
import ShoeComponent from './ShoeComponent';

function App() {
  return (
    <div className="App">
      <ShoeComponent/>
    </div>
  );
}

export default App;
