import React, { Component } from 'react'
import { connect } from "react-redux";
import ItemShoe from './ItemShoe';
import { ShoeReducer } from './redux/reducer/shoeReducer';

class ListShoe extends Component {
    renderShoeArr = () => {
        return this.props.dataShoe.map((item,index) => {
            return (
                <div className="col-4" key={index}>
                    <ItemShoe shoe={item}/>
                </div>
            )
        } )
    }
  render() {
    return (
        <div className="row container mx-auto">
            {this.renderShoeArr()}
        </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        dataShoe: state.ShoeReducer.shoeArr,
    }
}

export default connect(mapStateToProps)(ListShoe);