import React, { Component } from "react";
import { connect } from "react-redux";
import { DOWN_NUMBER, UP_NUMBER } from "./redux/constant/shoeConstane";

class Cart extends Component {
  renderCart = () => {
    return this.props.cartArr.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.number}</td>
          <td>
            <button onClick={() => {this.props.hanldDownNumber(item)}}>-</button>
            {item.number}
            <button onClick={() => {this.props.handleUpNumber(item)}}>+</button>
          </td>
          <td>
            <img style={{width:"100px"}} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Price</th>
              <th>Number</th>
              <th>Image</th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cartArr: state.ShoeReducer.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleUpNumber: (value) => {
            dispatch({
                type: UP_NUMBER,
                payload:value,
            })
        },
        hanldDownNumber: (value) => {
          dispatch({
            type: DOWN_NUMBER,
            payload: value,
          })
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Cart);
